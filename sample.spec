Name:       sample
Version:    1.0
Release:    1.0
Summary:    Most simple RPM package
License:    FIXME

Source0: sample-1.0.tar.gz

%description
Test

%prep
%setup -q
%autosetup -c


%install
mkdir -p %{buildroot}/usr/bin/
cp -r sample-1.0 %{buildroot}/usr/bin/


%files
/usr/bin/sample-1.0/sample1
/usr/bin/sample-1.0/sample1.deps.json
/usr/bin/sample-1.0/sample1.dll
/usr/bin/sample-1.0/sample1.pdb
/usr/bin/sample-1.0/sample1.runtimeconfig.json
/usr/bin/sample-1.0/sample.service